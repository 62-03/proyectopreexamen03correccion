package Modelo;

import java.sql.SQLException;
import java.util.ArrayList;

public class dbUsuarios extends dbManejador implements dbPersistencia {

    @Override
    public void insertar(Object objeto) throws Exception {
        Usuarios users = new Usuarios();
        users = (Usuarios) objeto;
        String consulta = "";
        consulta = "insert into " + "usuarios(nombre,correo,contraseña)values(?,?,?)";

        if (this.Conectar()) {
            try {
                System.err.println("se conecto");

                this.sqlConsulta = conexion.prepareStatement(consulta);
                //asignar valores a la consulta
                this.sqlConsulta.setString(1, users.getNombre());
                this.sqlConsulta.setString(2, users.getCorreo());
                this.sqlConsulta.setString(3, users.getContraseña());
                this.sqlConsulta.executeUpdate();
                this.Desconectar();
            } catch (SQLException e) {
                System.err.println("Surgio un error al insertar; " + e.getMessage());
            }
        }
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Usuarios> lista = new ArrayList<Usuarios>();
        Usuarios users;

        if (this.Conectar()) {
            String consulta = "SELECT * from usuarios order by correo";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            //Sacar los registros
            while (this.registros.next()) {
                users = new Usuarios();
                users.setIdUsuario(this.registros.getInt("idUsuarios"));
                users.setNombre(this.registros.getString("nombre"));
                users.setCorreo(this.registros.getString("correo"));
                users.setContraseña(this.registros.getString("contraseña"));
                lista.add(users);
            }
        }
        this.Desconectar();;
        return lista;
    }

    @Override
    public Object buscar(String nombre) throws Exception {
        Usuarios pro = new Usuarios();
        if (this.Conectar()) {
            String consulta = "SELECT * from usuarios where nombre = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            //Asignar valores
            this.sqlConsulta.setString(1, nombre);
            //Hacer la consulta
            this.registros = this.sqlConsulta.executeQuery();
            //Sacar los registros
            if (this.registros.next()) {
                pro.setIdUsuario(this.registros.getInt("idUsuarios"));
                pro.setNombre(this.registros.getString("nombre"));
                pro.setCorreo(this.registros.getString("correo"));
                pro.setContraseña(this.registros.getString("contraseña"));
            }
        }
        this.Desconectar();
        return pro;
    }

    @Override
    public boolean isExiste(String nombre) throws Exception {
        boolean res = false;
        Usuarios users = new Usuarios();
        if (this.Conectar()) {
            String consulta = "select * from usuarios where nombre =?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, nombre);
            this.registros = this.sqlConsulta.executeQuery();
            if (this.registros.next()) {
                res = true;
            }
        }
        this.Desconectar();
        return res;
    }

    @Override
    public boolean isCoincide(String nombre, String contraseña) throws Exception {
        boolean res = false;
        Usuarios users = new Usuarios();
        if (this.Conectar()) {

            try {
                String consulta = "select * from usuarios where nombre =? and contraseña =?";
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, nombre);
                this.sqlConsulta.setString(2, contraseña);
                this.registros = this.sqlConsulta.executeQuery();
                if (this.registros.next()) {
                    res = true;
                }
            } catch (SQLException e) {
                System.err.println("Surgio un error al insertar; " + e.getMessage());
            }

        }
        this.Desconectar();
        return res;
    }

}

package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Vista.*;
import Modelo.*;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
public class Controlador implements ActionListener{
    dlgIngreso ingreso;
    dlgLista lista;
    dlgRegistro registro;
    Usuarios usu;
    dbUsuarios dbUsu;

    public Controlador(dlgIngreso ingreso, dlgLista lista, dlgRegistro registro, Usuarios usu, dbUsuarios dbUsu) {
        this.ingreso = ingreso;
        this.lista = lista;
        this.registro = registro;
        this.usu = usu;
        this.dbUsu = dbUsu;
        
        ingreso.btnIngresar.addActionListener(this);
        ingreso.btnRegistrarse.addActionListener(this);
        lista.btnCerrar.addActionListener(this);
        registro.btnCerrar.addActionListener(this);
        registro.btnGuardar.addActionListener(this);
        registro.btnLimpiar.addActionListener(this);
    }
    private void limpiar(){
        registro.txtConfirmar.setText("");
        registro.txtContra.setText("");
        registro.txtCorreo.setText("");
        registro.txtUsuario.setText("");
    }
    private void iniVistaLista(){
        lista.setTitle(":: Lista ::");
        lista.setSize(500, 500);
        lista.setVisible(true);
    }
    private void iniVistaIngreso(){
        ingreso.setTitle("-- Ingreso --");
        ingreso.setSize(500,500);
        ingreso.setVisible(true);
    }
    private void iniVistaRegistro(){
        registro.setTitle("[] Registro []");
        registro.setSize(500,500);
        registro.setVisible(true);
    }
    private void mosTabla(){
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Usuarios> listas = new ArrayList<>();
        try {
            listas = dbUsu.listar();
        } catch (Exception ex) {
        }
        modelo.addColumn("idUsuarios");
        modelo.addColumn("nombre");
        modelo.addColumn("correo");
        modelo.addColumn("contraseña");
        for (Usuarios usuarios : listas) {
            modelo.addRow(new Object[]{usuarios.getIdUsuario(),usuarios.getNombre(), usuarios.getCorreo(), usuarios.getContraseña()});
        }
        lista.jtUsuarios.setModel(modelo);
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        dbUsuarios dbUsu = new dbUsuarios(); 
        dlgIngreso ingreso = new dlgIngreso(new JFrame(), true);
        dlgRegistro registro=new dlgRegistro(new JFrame(),true);
        dlgLista lista=new dlgLista(new JFrame(),true);
        Usuarios usu = new Usuarios();
        Controlador con = new Controlador(ingreso,lista,registro,usu,dbUsu);

        con.iniVistaIngreso();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==ingreso.btnRegistrarse){
            ingreso.setVisible(false);
            iniVistaRegistro();

        }
        if(e.getSource()==ingreso.btnIngresar){
            try {
                mosTabla();
                if(dbUsu.isCoincide(ingreso.txtUsuario.getText(), ingreso.txtContra.getText())){
                    ingreso.setVisible(false);
                    mosTabla();
                    iniVistaLista();
                    
                }
                else{
                    JOptionPane.showMessageDialog(ingreso, "Usuario o contraseña incorrecta...");
                }
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(ingreso, "No se pudo listar, surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                JOptionPane.showMessageDialog(ingreso, "No se pudo listar, surgio el siguiente error: "+ex2.getMessage());
            }
        }
        if(e.getSource()==registro.btnLimpiar){
            limpiar();
        }
        if(e.getSource()==registro.btnGuardar){ 
            try {
                 
                usu.setNombre(registro.txtUsuario.getText());
                usu.setCorreo(registro.txtCorreo.getText());
                usu.setContraseña(registro.txtContra.getText());
                if(usu.getContraseña().equals(registro.txtConfirmar.getText())){
                    if(dbUsu.isExiste(usu.getNombre())){
                        JOptionPane.showMessageDialog(registro, "El usuario ya existe");
                        limpiar();
                        return;
                    }
                    dbUsu.insertar(usu);
                    JOptionPane.showMessageDialog(registro, "Se agrego con exito");
                    limpiar();
                    return;
                }
                JOptionPane.showMessageDialog(registro, "Contraseña incorrecta");
                return;
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(registro, "No se pudo guardar, surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                JOptionPane.showMessageDialog(registro, "No se pudo guardar, surgio el siguiente error: "+ex2.getMessage());
            }
        }
        if(e.getSource()==registro.btnCerrar){
            int option=JOptionPane.showConfirmDialog(registro,"¿Deseas salir?",
            "Decide", JOptionPane.YES_NO_OPTION);
             if(option==JOptionPane.YES_NO_OPTION){
                 registro.setVisible(false);
                 iniVistaIngreso();
                 
             }
        }
        if(e.getSource()==lista.btnCerrar){
            int option=JOptionPane.showConfirmDialog(lista,"¿Deseas salir?",
            "Decide", JOptionPane.YES_NO_OPTION);
             if(option==JOptionPane.YES_NO_OPTION){
                 lista.setVisible(false);
                 iniVistaIngreso();
             }
        }
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}